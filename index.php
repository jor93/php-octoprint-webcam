<?php 
	// replace by your webcam URL
	$streamURL = "XXX";
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if (IE 9)]><html class="no-js ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-US"> <!--<![endif]-->

	<head>
	  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	  <meta name="description" content="Prusa MK3S Livestream" />
	  <meta name="author" content="Robert Johner">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">

	  <title>Prusa MK3S Livestream</title>
	  <!-- icon -->
	  <link rel="shortcut icon" href="assets/favicon.png" type="image/x-icon">	
	  <!-- bootstrap -->
	  <link rel="stylesheet" href="assets/bootstrap.min.css"> 
	  <!-- jquery -->
	  <script src="assets/jquery.min.js"></script>
	</head>

	<body>

		<div class="jumbotron text-center" style="padding: 1rem 1rem; margin-bottom: 1rem;">
		  <h1>Prusa MK3S</h1>
		</div>
		  
		<div class="container">
			<div class="row" style="margin-bottom: 1rem;">
				<h2>Livestream</h2>
				<div class="embed-responsive embed-responsive-16by9">				
					<iframe class="embed-responsive-item" style="background:#fff url('assets/loader.gif') no-repeat 50%;" scrolling="no" height="auto" width="auto" <?php echo "src=$streamURL"?> allowfullscreen>
						Your Browser does not support HTML5
					</iframe>
				</div>
			</div>
			<div class="row">
				<h2>Print Information</h2>
			</div>
			<div class="row">
				<div class="table-responsive">
				  <table class="table" id="placeholder">
				  </table>
				</div>
				<div class="alert alert-danger d-none" role="alert" id="errorMessage">
				  Backend not available!
				</div>
			</div>
		</div>
	
		<script>
			// variables
			var arr,
				printTime,
				printTimeLeft;
		
			displayLiveStats();
			// execute method every 30 seconds to update stats
			window.setInterval(function(){
				displayLiveStats();
			}, 30000);
			
			// execute method every 1 seconds to update timers
			window.setInterval(function(){
				if(arr !== undefined){
					// only update time if printing
					if(arr.state === "Printing from SD" || arr.state ==="Starting print from SD" || arr.state ==="Pausing"){					
						// increase print time by 1 second
						$("#printTime").text(formatDate(new Date(printTime.setUTCSeconds(printTime.getUTCSeconds()+1))));
						// check if time left is already avaiable from the backend
						if(arr.progress.printTimeLeft !== null){
							// decrease print time left by 1 second
							$("#printTimeLeft").text(formatDate(new Date(printTimeLeft.setUTCSeconds(printTimeLeft.getUTCSeconds()-1))));
						}
					}
				}
			}, 1000);
			
			// function to format date
			function formatDate(date) {
				var timeLeftModifier;
				if(date.getUTCHours()==0 && date.getUTCMinutes()==0){
					timeLeftModifier = "second(s)";
				} else 	if(date.getUTCHours()==0){
					timeLeftModifier = "minute(s)";
				} else {
					timeLeftModifier = "hour(s)";
				}
				return date.getUTCHours().toString().padStart(2, '0') + ':' + date.getUTCMinutes().toString().padStart(2, '0') + ':' + date.getSeconds().toString().padStart(2, '0') + " " + timeLeftModifier;
			}
			// function to display live stats in table
			function displayLiveStats() {	
				$.ajax({
						url : "octoprint.php",
						success : function(data) {
							// get table
							var table = $("#placeholder");
							// empty table
							table.empty();
							// check if error
							if(data==="error"){
								$("#errorMessage").removeClass("d-none");
								return;
							}
							// hide error message
							$("#errorMessage").addClass("d-none");
							// parse response
							arr = JSON.parse(data);

							// create dates
							printTime = new Date(arr.progress.printTime*1000);
							printTimeLeft = new Date(arr.progress.printTimeLeft*1000);
							 
							// check if status is printing, resuming, paused or pausing
							if(arr.state === "Printing from SD" || arr.state ==="Starting print from SD" || arr.state ==="Resuming" || arr.state === "Paused" || arr.state === "Pausing"){
								// add print informations to table 
								table.append("<tr><th><b>Printer Status:</b></th><td id='state'>"+arr.state+"</td></tr>" +
								"<tr><th><b>Progress:</b></th><td>" + "<div class='progress'><div class='progress-bar progress-bar-striped progress-bar-animated' role='progressbar' aria-valuenow='" +Math.round(arr.progress.completion)+ "' aria-valuemin='0' aria-valuemax='100' style='width:"+Math.round(arr.progress.completion)+"%;'>"+Math.round(arr.progress.completion)+"%</div></div></td></tr>" +	
								"<tr><th><b>Print Time Left:</b></th><td id='printTimeLeft'>"+(arr.progress.printTimeLeft !== null ? formatDate(printTimeLeft) : "-")+"</td></tr>"+	
								"<tr><th><b>Print Time Total:</b></th><td id='printTime'>"+formatDate(printTime)+"</td></tr>" +						
								"<tr><th><b>Print Job:</b></th><td>"+arr.job.file.name+"</td></tr>");
							} else {
								// add print status information to table
								table.append("<tr><th><b>Printer Status:</b></th><td id='state'>"+arr.state+"</td></tr>");
							}
							// check state to display correct state format
							if(arr.state === "Offline"){
								$("#state").toggleClass("text-secondary");
							} else if (arr.state === "Operational" || arr.state === "Printing from SD" || arr.state === "Starting print from SD" || arr.state === "Resuming") {
								$("#state").toggleClass("text-success");
							} else if (arr.state === "Pausing" || arr.state === "Paused") {
								$("#state").toggleClass("text-warning");	
							} else if (arr.state === "Cancelling" || arr.state === "Error") {
								$("#state").toggleClass("text-danger");
							}		
						},
						error: function(data) {
							// display error message
							$("#errorMessage").removeClass("d-none");
						},
						failure: function(data) {
							// display error message
							$("#errorMessage").removeClass("d-none");
						}
					});	
			}
		</script>
		
		<div id="footer">
		  <div class="container text-center">
			<p class="text-muted credit" style="color:#fff">&copy;<?php echo date("Y"); ?> done with &hearts; by Robert Johner</p>
		  </div>
		</div>
	</body>
</html>
