<?php 
	// replace by your octoprint url
	$octoPrintURL = "XXX";
	// replace by your octoprint API key
	$api= "XXX";

	$curl_h = curl_init($octoPrintURL);
	curl_setopt($curl_h, CURLOPT_FAILONERROR, true);
	curl_setopt($curl_h, CURLOPT_HTTPHEADER,
		array(
			'X-Api-Key:' . $api
		)
	);

	$response = curl_exec($curl_h);

	if($response === false){
		echo "error";
	}
	return json_encode($response);
?>