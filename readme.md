
# 3D Printer Live Video Feed
A web based video live feed for your 3D Printer to monitor your 3D prints remotely. The web application can be connected to a local OctoPrint instance to show detailed information about the print itself. For the live video feed, Shinobi is used to securely stream the video feed to the web application:
<p align="center">
 <img src="img/octoprint.png?raw=true" alt="Skin"/>
</p>

Watch it live: https://prusa.robertjohner.ch/


# What you need?
 1. A public webserver 
 2. OctoPrint server instance
 3. Video Feed available over http

# Software!
 1. All you need to do is replace the three variables in the files "index.php" and "octoprint.php". Each variable is explained in the file.
 


*made with ♥ by jor
